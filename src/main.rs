#![feature(test)]

extern crate rayon;
extern crate rand;
#[cfg(test)]
extern crate test;

#[cfg(test)]
use test::Bencher;
use rayon::prelude::*;

fn main() { }

#[test]
fn max_int() {
    let mut random_values = vec![
        17,
        12,
        42,
        23,
        0,
        9087234,
        0,
        0x9234,
        2308947,
        std::usize::MAX,
        3,
        1,
        4,
        1,
        5,
        9,
    ];
    let mut expected = random_values.clone();
    expected.sort();

    println!("Input:  {:?}", random_values);

    our_quack_bubble_sort(&mut random_values);

    println!("Result: {:?}", random_values);

    assert_eq!(expected, random_values);
}


#[test]
fn just10() {
    let mut random_values = vec![
        17, 100000000000, 12, 2308947, 2,
        2308947, 2, 4, 6, 8,
    ];
    let mut expected = random_values.clone();
    expected.sort();

    our_quack_bubble_sort(&mut random_values);

    assert_eq!(expected, random_values);
}

#[bench]
fn just10normal(bencher: &mut Bencher) {
    let random_values = vec![
        17, 42, 12, 2308947, 2,
        2308947, 2, 4, 6, 8,
    ];
    let mut working_values = random_values.clone();
    let mut expected = random_values.clone();
    expected.sort();
    bencher.iter(|| {
        working_values.copy_from_slice(&random_values);

        our_quack_bubble_sort(&mut working_values);
    });
    assert_eq!(expected, working_values);
}


#[bench]
fn random_million(bencher: &mut Bencher) {
    let random_values = (0..1000_000)
        .map(|_| rand::random())
        .collect::<Vec<usize>>();
    let mut working_values = random_values.clone();
    let mut expected = random_values.clone();
    expected.sort();
    bencher.iter(|| {
        working_values.copy_from_slice(&random_values);

        our_quack_bubble_sort(&mut working_values);
    });
    assert_eq!(expected, working_values);
}

#[bench]
fn random_million_std(bencher: &mut Bencher) {
    let random_values = (0..1000_000)
        .map(|_| rand::random())
        .collect::<Vec<usize>>();
    let mut working_values = random_values.clone();
    let mut expected = random_values.clone();
    expected.sort();
    bencher.iter(|| {
        working_values.copy_from_slice(&random_values);

        working_values.sort();
    });
    assert_eq!(expected, working_values);
}

#[bench]
fn random_million_rayon(bencher: &mut Bencher) {
    let random_values = (0..1000_000)
        .map(|_| rand::random())
        .collect::<Vec<usize>>();
    let mut working_values = random_values.clone();
    let mut expected = random_values.clone();
    expected.sort();
    bencher.iter(|| {
        working_values.copy_from_slice(&random_values);

        working_values.par_sort();
    });
    assert_eq!(expected, working_values);
}

const SPLIT_SIZE: usize = 50;
pub fn our_quack_bubble_sort(data: &mut [usize]) {
    let mut buf = vec![0; data.len()];
    our_quack_bubble_sort_inner(data, &mut buf);
}
fn our_quack_bubble_sort_inner(data: &mut [usize], buf: &mut [usize]) {
    let length = data.len();
    if length > SPLIT_SIZE {
        {
            let (left, right) = data.split_at_mut(length / 2);
            {
                let (left_buf, right_buf) = buf.split_at_mut(length / 2);
                rayon::join(
                    || our_quack_bubble_sort_inner(left, left_buf),
                    || our_quack_bubble_sort_inner(right, right_buf),
                );
            }

            // Quacky Merge Sort
            let mut left_i = 0;
            let mut right_i = 0;
            for e in buf.iter_mut() {
                let left_c = left.get(left_i);
                let right_c = right.get(right_i);
                *e = *match (left_c, right_c) {
                    (Some(l), Some(r)) => {
                        if l < r {
                            left_i += 1;
                            l
                        } else {
                            right_i += 1;
                            r
                        }
                    }
                    (None, Some(r)) => {
                        right_i += 1;
                        r
                    }
                    (Some(l), None) => {
                        left_i += 1;
                        l
                    },
                    _ => unreachable!("out of bounds can not happen"),
                }
            }
        }
        data.copy_from_slice(buf);
    } else {
        // actually do work
        data.sort();
    }
}
